# Blinders Browser Extension

Hides/shows distracting elements on select websites. Free your mind.

Replaces distracting feeds behind a button on the following sites:

- YouTube
- Twitter
- Hacker News

**Package add-on:** `zip -r -FS ./blinders.zip * --exclude '*.git*'`
