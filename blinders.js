const location = window.location.href;
let query = null;

// Hacker News
if (
  location === "https://news.ycombinator.com/" ||
  location === "https://news.ycombinator.com/news"
) {
  query = ".itemlist";
}

// YouTube
if (location === "https://www.youtube.com/") {
  query = "#primary";
}

// Twitter
if (location === "https://twitter.com/home") {
  query = 'div[data-testid="primaryColumn"]';
}

if (query !== null) {
  let targets = document.body.querySelectorAll(query);
  let counter = 0;

  // Keep trying for awhile if element hasn't appeared
  // TODO: Look into using MutationObserver instead
  if (targets.length === 0) {
    let interval = setInterval(function () {
      counter += 1;
      targets = document.body.querySelectorAll(query);
      if (targets.length > 0) {
        clearInterval(interval);
        init(targets);
      }
      if (counter >= 20) {
        clearInterval(interval);
      }
    }, 250);
  } else {
    init(targets);
  }
}

function insertBefore(el, referenceNode) {
  referenceNode.parentNode.insertBefore(el, referenceNode);
}

function init(targets) {
  targets.forEach(function (target) {
    const targetTagname = target.tagName;
    const targetDisplayValue = target.style.display;
    const replacement = document.createElement("div");

    target.style.display = "none";

    replacement.classList.add("blinders-extention");
    replacement.innerHTML = `Content hidden by <span class="blinders-extention-dib">Blinders extention</span> <span class="blinders-extention-secondary blinders-extention-dib">(Click to unhide)</span>`;

    replacement.addEventListener("click", function () {
      replacement.parentNode.removeChild(replacement);
      target.style.display = targetDisplayValue;
    });

    insertBefore(replacement, target);
  });
}
